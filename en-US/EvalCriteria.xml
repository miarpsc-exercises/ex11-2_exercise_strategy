<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE Article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>
<appendix>
  <title>Preparedness Criteria</title>
  <para>
    the following tables show the FEMA criteria for evaluating
    readiness against the capabilities to be tested.  Note that the
    preparedness criteria address multiple capabilities, and many of
    the criteria are not applicable to ARES/RACES.
  </para>
  <para>
    <table frame="all">
      <title>Preparedness Criteria for Com.A2</title>
      <tgroup cols="2">
	<colspec colnum="1" colname="c1" colwidth="6*" />
	<colspec colnum="2" colname="c2" colwidth="1*" />
	<thead>
	  <row>
	    <entry>Preparedness Measures</entry>
	    <entry>Metric</entry>
	  </row>
	</thead>
	<tbody>

	  <row>
	    <entry>
	      Continuity of Operation (COOP) plans describe how
	      personnel, equipment, and other resources support
	      sustained response/survivability and recovery for all
	      sectors
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	    Continuity of Government (COG) plans describe the
	    continued functioning of constitutional government under
	    all circumstances
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	    Emergency response plans are consistent with the National
	    Response Plan (NRP) and National Incident Management
	    System (NIMS)
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	    Mutual aid assistance agreements are in place with
	    contiguous jurisdictions
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	    Preparedness plans are consistent with NRP and
	    NIMS
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Aid assistance agreements or contracts with private
	      organizations are in place
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Pre-identified mechanisms to request assistance from
	      counties, the State, or the Federal Government are in
	      place
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Emergency response plans address substantial loss of
	      public safety response capabilities during catastrophic
	      events (to include special needs populations and people
	      with disabilities)
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Frequency with which plans are reviewed and updated to
	      ensure compliance with governmental regulations and
	      policies (Review requirements are intended to apply only
	      when no pre-existing review cycle has been established
	      in Federal, State, or local requirements)
	    </entry>
	    <entry>12 months</entry>
	  </row>

	</tbody>
      </tgroup>
    </table>
  </para>

  <para>
    <table frame="all">
      <title>Preparedness Criteria for Com.C2</title>
      <tgroup cols="2">
	<colspec colnum="1" colname="c1" colwidth="6*" />
	<colspec colnum="2" colname="c2" colwidth="1*" />
	<thead>
	  <row>
	    <entry>Preparedness Measures</entry>
	    <entry>Metric</entry>
	  </row>
	</thead>
	<tbody>
	  <row>
	    <entry>
	      Operable communications systems that are supported by
	      redundancy and diversity, that provide service across
	      jurisdictions, and that meet everyday internal agency
	      requirements, are in place
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Communication systems support on-demand, real-time
	      interoperable voice and data communication
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans and procedures are in place to ensure appropriate
	      levels of planning and building public safety
	      communication systems prior to an incident
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans and procedures are in place to ensure appropriate
	      levels of upgrading/enhancing public safety
	      communication systems and equipment prior to an
	      incident
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans and procedures are in place to ensure appropriate
	      levels of replacing public safety communication systems
	      and equipment prior to an incident
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans and procedures are in place to ensure appropriate
	      levels of maintaining public safety communication
	      systems and equipment prior to an incident 
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans and procedures are in place to ensure appropriate
	      levels of managing public safety communication projects
	      prior to an incident 
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Assessment of standard communication capabilities for
	      Public Safety Answering Points (PSAP)/Public Safety
	      Communication Centers and Emergency Operations Centers
	      (EOC) to ensure appropriate Continuity of Operations
	      Plan (COOP) for public safety and service agencies'
	      communications has been completed 
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Communications Continuity of Operations Plan (COOP) that
	      outlines back-up systems available at State and local
	      levels, including protocols for use of systems, is in
	      place
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Communications standard operating procedures (SOPs) that
	      conform to NIMS are in place and are used in routine
	      multiple jurisdictional responses
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Interoperability policies and procedures to allow
	      information sharing between levels of government and
	      Federal installations involved in incident, as necessary
	      and as possible, are in place
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Redundant and diverse interoperable communication
	      systems are available
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans to coordinate the procurement of communications
	      assets to ensure interoperability are in place
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans to acquire and influence sustained
	      interoperability and systems maintenance funding have
	      been developed 
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Plans include a procedure to return communications back
	      to normal operations after each significant
	      incident
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      A multi-agency and multi-jurisdictional governance
	      structure to improve communications interoperability
	      planning and coordination has been established
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Formal interoperable communications agreements have been
	      established through the governance structure
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	  <row>
	    <entry>
	      Interoperability communications plans have been
	      developed through governance structure and include all
	      relevant agencies for data and voice
	      communications.
	    </entry>
	    <entry>Yes/No</entry>
	  </row>

	</tbody>
      </tgroup>
    </table>
  </para>

</appendix>

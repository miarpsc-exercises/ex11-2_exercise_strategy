<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE Article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>
<appendix>
  <title>Communications</title>
  <table frame="all">
    <title>Common C - Communications</title>
    <tgroup cols="2">
      <colspec colnum="1" colname="c1" colwidth="1*" />
      <colspec colnum="2" colname="c2" colwidth="4*" />
      <tbody>
        <row>
          <entry>ComC 1</entry>
          <entry>Develop communication plans, policies, procedures, and systems that support required communications with all Federal, regional, State, local and tribal governments and agencies as well as voluntary agencies</entry>
        </row>
        <row>
          <entry>ComC 1.1</entry>
          <entry>Establish policies and procedures for communications and warnings</entry>
        </row>
        <row>
          <entry>ComC 1.1.1</entry>
          <entry>Develop a continuous improvement plan that enriches interoperable communications to provide advanced customer service, reliability, and operational effectiveness</entry>
        </row>
        <row>
          <entry>ComC 1.2</entry>
          <entry>Develop interoperability and compatibility criteria</entry>
        </row>
        <row>
          <entry>ComC 1.2.1</entry>
          <entry>Develop procedures for the exchange of voice and data with Federal, regional, State, local and tribal agencies as well as voluntary agencies</entry>
        </row>
        <row>
          <entry>ComC 1.2.2</entry>
          <entry>Develop common communication and data standards to facilitate the exchange of information in support of response management</entry>
        </row>
        <row>
          <entry>ComC 1.2.2.1</entry>
          <entry>Establish common response communications language</entry>
        </row>
        <row>
          <entry>ComC 1.2.2.2</entry>
          <entry>Develop a standard set of data elements for sharing information (e.g., status and pollution) across regional, State and local agencies</entry>
        </row>
        <row>
          <entry>ComC 1.2.2.3</entry>
          <entry>Facilitate the development of sampling and data collection information exchange standards</entry>
        </row>
        <row>
          <entry>ComC 1.2.2.4</entry>
          <entry>Facilitate the development of geospatial information exchange standards</entry>
        </row>
        <row>
          <entry>ComC 1.2.3</entry>
          <entry>Facilitate the development of wireless communication and computer procedures and protocols to permit interoperability between government and local public safety organizations</entry>
        </row>
        <row>
          <entry>ComC 1.3</entry>
          <entry>Establish and maintain information systems across response entities</entry>
        </row>
        <row>
          <entry>ComC 1.3.1</entry>
          <entry>Develop interoperable telecommunication and Information Technology systems across governmental departments and agencies</entry>
        </row>
        <row>
          <entry>ComC 1.3.2</entry>
          <entry>Develop and maintain surveillance and detection systems</entry>
        </row>
        <row>
          <entry>ComC 1.3.2.1</entry>
          <entry>Develop and maintain geographic information systems (GIS)</entry>
        </row>
        <row>
          <entry>ComC 1.3.2.2</entry>
          <entry>Develop and maintain the health alert network</entry>
        </row>
        <row>
          <entry>ComC 1.3.2.3</entry>
          <entry>Establish role of National Biosurveillance Integration System (NBIS) at the EOC</entry>
        </row>
        <row>
          <entry>ComC 1.3.4</entry>
          <entry>Coordinate with telecommunications service providers to ensure all telecommunications service requirements are satisfied</entry>
        </row>
        <row>
          <entry>ComC 1.4</entry>
          <entry>Design reliable, redundant, and robust communications systems for daily operations capable of quickly reconstituting normal operations in the event of disruption or destruction</entry>
        </row>
        <row>
          <entry>ComC 1.4.1</entry>
          <entry>Design reliable, redundant, and robust communications systems for daily operations capable of quickly reconstituting normal operations in the event of disruption or destruction</entry>
        </row>
        <row>
          <entry>ComC 1.4.1.1</entry>
          <entry>Establish role of the operation area satellite system (OASIS) at the EOC</entry>
        </row>
        <row>
          <entry>ComC 1.4.2</entry>
          <entry>Establish a secure and redundant communications system that ensures connectivity between health care facilities and health departments, emergency medical services, emergency management agencies, public safety agencies, blood collection agencies, building departments, neighboring jurisdictions and Federal health officials</entry>
        </row>
        <row>
          <entry>ComC 1.4.3</entry>
          <entry>Establish a national database of incident reports to support response management efforts and analysis</entry>
        </row>
        <row>
          <entry>ComC 1.5</entry>
          <entry>Develop information systems protection procedures</entry>
        </row>
        <row>
          <entry>ComC 1.5.1</entry>
          <entry>Develop and maintain automated credential verification systems to ensure proper credentialing for controlled access areas</entry>
        </row>
        <row>
          <entry>ComC 1.5.2</entry>
          <entry>Coordinate the maintenance and safeguarding of key records, building plans and documents</entry>
        </row>
        <row>
          <entry>ComC 1.5.3</entry>
          <entry>Establish a national authentication and security identification certification system for emergency responders, Federal, State, local and tribal personnel and other nongovernmental personnel requiring access to affected areas</entry>
        </row>
        <row>
          <entry>ComC 1.6</entry>
          <entry>Develop supplemental and back-up communications and information technology plans, procedures, and systems</entry>
        </row>
        <row>
          <entry>ComC 1.6.1</entry>
          <entry>Promote and facilitate the development of redundant communications networks</entry>
        </row>
        <row>
          <entry>ComC 1.6.2</entry>
          <entry>Identify emergency communications and data requirements for each stakeholder</entry>
        </row>
        <row>
          <entry>ComC 1.6.3</entry>
          <entry>Identify emergency communications staff roles and responsibilities</entry>
        </row>
        <row>
          <entry>ComC 1.6.4</entry>
          <entry>Identify available operational telecommunication assets needs for use on and off-incident site</entry>
        </row>
        <row>
          <entry>ComC 1.6.5</entry>
          <entry>Complete an assessment of standard communication capabilities for the Public Safety Answering Points (PSAPs) and Public Safety Communication Centers to ensure an appropriate Continuity of Operations Plan (COOP) is in place for public safety and service agencies' communications</entry>
        </row>
        <row>
          <entry>ComC 1.7</entry>
          <entry>Implement the national telecommunication support plan</entry>
        </row>
        <row>
          <entry>ComC 1.7.1</entry>
          <entry>Identify priority telecommunications programs and services</entry>
        </row>
        <row>
          <entry>ComC 1.7.2</entry>
          <entry>Coordinate procurement and placement of technology communication systems based on a gap analysis of requirements versus existing capabilities</entry>
        </row>
        <row>
          <entry>ComC 1.7.3</entry>
          <entry>Develop plans to provide telecommunication and information technology support to Federal, regional, State, local and tribal officials and the private sector</entry>
        </row>
        <row>
          <entry>ComC 2</entry>
          <entry>Develop and Implement Training and Exercise Programs for Response Communications</entry>
        </row>
        <row>
          <entry>ComC 2.1</entry>
          <entry>Develop and Implement Training Programs for Response Communications</entry>
        </row>
        <row>
          <entry>ComC 2.1.1</entry>
          <entry>Develop and implement awareness training programs for response communications</entry>
        </row>
        <row>
          <entry>ComC 2.1.2</entry>
          <entry>Develop exercises/drills of sufficient intensity to challenge management and operations and to test the knowledge, skills, and abilities of individuals and organizations for response communications</entry>
        </row>
        <row>
          <entry>ComC 2.2</entry>
          <entry>Develop and Implement Exercise Programs for Response Communications</entry>
        </row>
        <row>
          <entry>ComC 2.2.1</entry>
          <entry>Develop and conduct training to improve all-hazard incident management capability for response communications</entry>
        </row>
        <row>
          <entry>ComC 2.2.2</entry>
          <entry>Conduct an after action review to determine strengths and shortfalls and develop a corrective plan accordingly for response communications</entry>
        </row>
        <row>
          <entry>ComC 3</entry>
          <entry>Conduct alert and dispatch notification</entry>
        </row>
        <row>
          <entry>ComC 3.1</entry>
          <entry>Dispatch first responders</entry>
        </row>
        <row>
          <entry>ComC 3.2</entry>
          <entry>Dispatch secondary response agencies</entry>
        </row>
        <row>
          <entry>ComC 3.3</entry>
          <entry>Implement government and NGO agency notification protocols and procedures</entry>
        </row>
        <row>
          <entry>ComC 3.4</entry>
          <entry>Request external resources using EMAC and other mutual aid/assistance processes (inter- and intra-State)</entry>
        </row>
        <row>
          <entry>ComC 4</entry>
          <entry>Provide Incident Command/First Responder/First Receiver/Interoperable Communications</entry>
        </row>
        <row>
          <entry>ComC 4.1</entry>
          <entry>Establish and maintain response communications systems on-site</entry>
        </row>
        <row>
          <entry>ComC 4.1.1</entry>
          <entry>Verify immediately that critical communication links among first responders are functioning</entry>
        </row>
        <row>
          <entry>ComC 4.2</entry>
          <entry>Implement response communications interoperability plans and protocols</entry>
        </row>
        <row>
          <entry>ComC 4.2.1</entry>
          <entry>Communicate internal incident response information</entry>
        </row>
        <row>
          <entry>ComC 4.2.1.1</entry>
          <entry>Use established common response communications language (i.e., plain English)</entry>
        </row>
        <row>
          <entry>ComC 4.2.2</entry>
          <entry>Coordinate incident site communications within a National Incident Management System (NIMS) compliant framework</entry>
        </row>
        <row>
          <entry>ComC 4.2.3</entry>
          <entry>Report and document the incident by completing and submitting required forms, reports, documentation, and follow-up notations on immediate response communications</entry>
        </row>
        <row>
          <entry>ComC 4.3</entry>
          <entry>Implement information systems protection procedures</entry>
        </row>
        <row>
          <entry>ComC 5</entry>
          <entry>Provide EOC Communications Support</entry>
        </row>
        <row>
          <entry>ComC 5.1</entry>
          <entry>Develop NIMS compliant incident site communications plan</entry>
        </row>
        <row>
          <entry>ComC 5.2</entry>
          <entry>Establish and maintain communications organization/operation with EOC/MACC</entry>
        </row>
        <row>
          <entry>ComC 5.3</entry>
          <entry>Establish and maintain interoperable information systems within EOC</entry>
        </row>
        <row>
          <entry>ComC 5.3.1</entry>
          <entry>Establish communications resource requirements</entry>
        </row>
        <row>
          <entry>ComC 5.3.1.1</entry>
          <entry>Coordinate placement of latest technology that is available to agencies participating in response</entry>
        </row>
        <row>
          <entry>ComC 5.3.1.2</entry>
          <entry>Coordinate and provide telecommunication and information technology support to Federal, regional, State, local and tribal officials and the private sector</entry>
        </row>
        <row>
          <entry>ComC 5.3.1.3</entry>
          <entry>Coordinate and open State communications support/channels to local and tribal government and the private-sector to assist in awareness, prevention, response and recovery communication activities</entry>
        </row>
        <row>
          <entry>ComC 5.3.2</entry>
          <entry>Implement communication security procedures and systems</entry>
        </row>
        <row>
          <entry>ComC 5.3.3</entry>
          <entry>Assure redundant communications circuits/channels are available for use</entry>
        </row>
        <row>
          <entry>ComC 5.3.4</entry>
          <entry>Activate back-up information systems as needed</entry>
        </row>
        <row>
          <entry>ComC 5.4</entry>
          <entry>Coordinate communications policy and procedures across response entities</entry>
        </row>
        <row>
          <entry>ComC 5.4.1</entry>
          <entry>Disseminate information to emergency managers and responders</entry>
        </row>
        <row>
          <entry>ComC 5.4.2</entry>
          <entry>Coordinate with the response organization and other responders to share information</entry>
        </row>
        <row>
          <entry>ComC 5.4.3</entry>
          <entry>Coordinate information transfer between and among Incident Command Post</entry>
        </row>
        <row>
          <entry>ComC 5.4.4</entry>
          <entry>Coordinate information transfer from the incident scene to the MACS (e.g. Emergency Operations Center (EOC))</entry>
        </row>
        <row>
          <entry>ComC 5.4.5</entry>
          <entry>Provide direction, information, and/or support as appropriate to the incident command (IC), unified command (UC), and/or joint field office(s)</entry>
        </row>
        <row>
          <entry>ComC 5.4.6</entry>
          <entry>Provide response information across jurisdictional boundaries</entry>
        </row>
        <row>
          <entry>ComC 5.5</entry>
          <entry>Maintain a common operating picture (COP) for real time sharing of information with all the participating entities to ensure all responder agencies are working from the same information</entry>
        </row>
        <row>
          <entry>ComC 5.6</entry>
          <entry>Monitor communications and information systems</entry>
        </row>
        <row>
          <entry>ComC 5.6.1</entry>
          <entry>Develop incident log</entry>
        </row>
        <row>
          <entry>ComC 5.6.2</entry>
          <entry>Update responder information</entry>
        </row>
        <row>
          <entry>ComC 6</entry>
          <entry>Provide Federal Facilities, Task Force, and Recovery Assistance Interoperable Communications</entry>
        </row>
        <row>
          <entry>ComC 7</entry>
          <entry>Return communication system to normal operations</entry>
        </row>
        <row>
          <entry>ComC 7.1</entry>
          <entry>Initiate interoperable deactivation procedures</entry>
        </row>
        <row>
          <entry>ComC 7.2</entry>
          <entry>Assist in deactivation of telecommunication resources and assets .</entry>
        </row>
        <row>
          <entry>ComC 7.3</entry>
          <entry>Maintain audit and reports on all telecommunications support provided.</entry>
        </row>
      </tbody>
    </tgroup>
  </table>
</appendix>
